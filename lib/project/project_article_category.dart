import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/model/article_category_bean.dart';
import 'package:flutter_app/widget/my_circular_progress_indicator.dart';

import 'article_list.dart';
///项目
class ProjectPage extends StatefulWidget {
  @override
  ProjectPageState createState() => new ProjectPageState();
}

class ProjectPageState extends State<ProjectPage> {
  var _tabList = List<Tab>();
  var _tabBarView=List<Widget>();
  @override
  void initState() {
    _getProjectArticleCategoryList();
    super.initState();
  }

  void _getProjectArticleCategoryList() async{
    ApiService().getProjectArticleCategory((ArticleCategoryBean articleCategoryBean){
      setState(() {
        _tabList.clear();
        _tabBarView.clear();
        articleCategoryBean.category.forEach((element) {
          _tabList.add(Tab(text: element.name));
          _tabBarView.add(ProjectArticleListPage(element.id));
        });
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    if(_tabList.length==0){
        return Center(
          child: MyCircularProgressIndicator(),
        );
    }else {
      return MaterialApp(
        home: DefaultTabController(
          length: _tabList.length,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(50.0),
              child: AppBar(
                bottom: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.white.withOpacity(0.6),
                  indicatorColor: Colors.white,
                  tabs: _tabList,
                ),
              ),
            ),
            body: TabBarView(
              children: _tabBarView,
            ),
          ),
        ),
      );
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
