import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/model/article_bean.dart';
import 'package:flutter_app/res/strings.dart';
import 'package:flutter_app/webview/WebViewPage.dart';
import 'package:flutter_app/widget/my_circular_progress_indicator.dart';

//项目文章列表
// ignore: must_be_immutable
class ProjectArticleListPage extends StatefulWidget {
  int id;

  ProjectArticleListPage(int id) {
    this.id = id;
  }

  @override
  ProjectArticleListPageState createState() =>
      new ProjectArticleListPageState(id);
}

class ProjectArticleListPageState extends State<ProjectArticleListPage> {
  List<Article> _articleList = List();
  int _projectCategoryId;
  int _pageNumber = 0;
  bool _isLoadAllArticles = false;

  ProjectArticleListPageState(int _projectCategoryId) {
    this._projectCategoryId = _projectCategoryId;
  }

  void _getProjectArticleData(int pageNumber) async {
    ApiService().getProjectArticleData(pageNumber, _projectCategoryId,
        (ArticleBean articleBean) {
      setState(() {
        if (articleBean.data != null) {
          _isLoadAllArticles = articleBean.data.over;
          _articleList.addAll(articleBean.data.articles);
        }
        if (_isLoadAllArticles) {
          _articleList.add(null);
        }
      });
    });
  }

  @override
  void initState() {
    _getProjectArticleData(_pageNumber);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: _articleList.length,
        itemBuilder: (cxt, index) {
          if (index == _articleList.length - 1) {
            //加载了所有数据后，不必再去请求服务器，这时候也不应该展示 loading, 而是展示"所有文章都已被加载"
            if (!_isLoadAllArticles) {
              _getProjectArticleData(++_pageNumber);
              return MyCircularProgressIndicator();
            } else {
              return Container(
                padding: EdgeInsets.all(5),
                alignment: Alignment.center,
                child: Text(
                  Strings.IS_LOAD_ALL_ARTICLE_CN,
                ),
              );
            }
          }
          return GestureDetector(
            child: _projectArticleListItem(_articleList[index]),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>WebViewPage(_articleList[index].link)));
            },
          );
        },
        scrollDirection: Axis.vertical,
      ),
    );
  }

  Widget _projectArticleListItem(Article article) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
                color: Colors.grey, offset: Offset(2.0, 2.0), blurRadius: 4.0)
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image(
            image: NetworkImage(article.envelopePic),
            width: 110,
            height: 150,
            alignment: Alignment.center,
          ),
          Expanded(
              child: Padding(
            padding: EdgeInsets.only(left: 10, right: 5, bottom: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  article.title,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                Text(
                  article.desc,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Text(
                        article.author.isEmpty
                            ? article.shareUser
                            : article.author,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(
                            article.niceDate,
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.normal,
                                color: Colors.grey),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
