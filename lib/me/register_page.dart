import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter_app/model/user_bean.dart';
import 'package:flutter_app/utils/constant.dart';
import 'package:flutter_app/utils/sp_util.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegisterPage extends StatefulWidget {
  @override
  RegisterPageState createState() => new RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  final _userNameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _ensurePasswordController = TextEditingController();
  bool _isUserNameContent = false;
  bool _isPwdContent = false;
  bool _isEnPwdContent = false;

  @override
  void initState() {
    _userNameController.addListener(() {
      if(_userNameController.text.trim().isNotEmpty){
        _isUserNameContent=true;
      }else{
        _isUserNameContent=false;
      }
    });
    _passwordController.addListener(() {
      if(_passwordController.text.trim().isNotEmpty){
        _isPwdContent=true;
      }else{
        _isPwdContent=false;
      }
    });
    _ensurePasswordController.addListener(() {
      if(_ensurePasswordController.text.trim().isNotEmpty){
        _isEnPwdContent=true;
      }else{
       _isEnPwdContent=false;
      }
    });
    super.initState();
  }
 void _register() async{
    var data={
      "username":_userNameController.text,
      "password":_userNameController.text,
      "repassword":_userNameController.text,
    };
    ApiService().register(data, (UserBean userBean){
      if(userBean.errorCode==0){
        SpUtil().putString(Constant.usernameTag,_userNameController.text);
        SpUtil().putBool(Constant.isLoginKey, true);
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>MyApp()), (route) => false);
        Fluttertoast.showToast(msg: "注册成功");
      }else{
        Fluttertoast.showToast(msg: "注册失败:${userBean.errorMsg}");
      }
    });
 }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey,
        image: DecorationImage(
            image: AssetImage("images/landscape.jpg"),
            colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.2), BlendMode.dstATop),
            fit: BoxFit.fill),
      ),
      child: Scaffold(
        appBar: AppBar(
          elevation: 5,
          backgroundColor: Colors.transparent,
          title: Text(
            "注册",
            style: TextStyle(fontSize: 18),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 50, right: 25, left: 25),
          child: Column(
            children: [
              TextField(
                controller: _userNameController,
                textInputAction: TextInputAction.go,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    labelText: "用户名",
                    hintText: "请输入用户名",
                    prefixIcon: Icon(Icons.account_circle),
                    suffixIcon: Visibility(
                      child: IconButton(
                        icon: Icon(Icons.clear),
                        alignment: Alignment.bottomCenter,
                        padding: EdgeInsets.all(1),
                        onPressed: () {},
                      ),
                      visible: _isUserNameContent,
                    )),
              ),
              TextField(
                controller: _passwordController,
                textInputAction: TextInputAction.go, ////用于控制键盘动作（一般位于右下角，默认是完成）
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    labelText: "密码",
                    hintText: "请输入密码",
                    prefixIcon: Icon(Icons.lock),
                    suffixIcon: Visibility(
                      child: IconButton(
                        icon: Icon(Icons.clear),
                        alignment: Alignment.bottomCenter,
                        padding: EdgeInsets.all(1),
                        onPressed: () {},
                      ),
                      visible: _isPwdContent,
                    )),
              ),
              TextField(
                controller: _ensurePasswordController,
                textInputAction: TextInputAction.go,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    labelText: "确认密码",
                    hintText: "请确认密码",
                    prefixIcon: Icon(Icons.looks_two_outlined),
                    suffixIcon: Visibility(
                      child: IconButton(
                        icon: Icon(Icons.clear),
                        alignment: Alignment.bottomCenter,
                        padding: EdgeInsets.all(1),
                        onPressed: () {},
                      ),
                      visible: _isEnPwdContent,
                    )),
              ),
              Padding(
                padding: EdgeInsets.only(top: 25),
                child: RaisedButton(
                  child: Text("注册"),
                  color: Colors.white54,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  onPressed: () {
                    if(_isUserNameContent&&_isPwdContent&&_isEnPwdContent){
                      if(_passwordController.text==_ensurePasswordController.text){
                        _register();
                      }else{
                        Fluttertoast.showToast(msg: "两次密码输入不一致");
                      }
                    }else if(!_isUserNameContent){
                      Fluttertoast.showToast(msg: "请输如用户名");
                    }else if(!_isPwdContent){
                      Fluttertoast.showToast(msg: "请输入密码");
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


  @override
  void dispose() {
    _userNameController.dispose();
    _passwordController.dispose();
    _ensurePasswordController.dispose();
    super.dispose();
  }
}
