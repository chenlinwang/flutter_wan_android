import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/home/home.dart';
import 'package:flutter_app/model/article_bean.dart';
import 'package:flutter_app/res/strings.dart';
import 'package:flutter_app/utils/constant.dart';
import 'package:flutter_app/utils/sp_util.dart';
import 'package:flutter_app/webview/WebViewPage.dart';
import 'package:flutter_app/widget/my_circular_progress_indicator.dart';

class SearchResultPage extends StatefulWidget {
  const SearchResultPage({Key key, this.searchKeyWords}) : super(key: key);

  final String searchKeyWords;

  @override
  SearchResultPageState createState() => new SearchResultPageState();
}

class SearchResultPageState extends State<SearchResultPage> {
  String searchKeyWords;
  int pageNumber = 0;
  bool isLoadAll = false;
  List<Article> _articleList = List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(searchKeyWords),
      ),
      body: (_articleList.length == 0)
          ? Center(child: MyCircularProgressIndicator())
          : SafeArea(child: ListView.builder(itemBuilder: (context, index) {
            if(index==_articleList.length-1){
              if(!isLoadAll){
                _search(++pageNumber);
                return MyCircularProgressIndicator();
              }else{
                return Container(
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.center,
                  child: Text(
                    Strings.IS_LOAD_ALL_ARTICLE_CN
                  ),
                );
              }
            }else{
              return GestureDetector(
                child: ArticleListItemWidget(_articleList[index]),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>WebViewPage(_articleList[index].link)));
                },
              );
            }
      })),
    );
  }

  @override
  void initState() {
    searchKeyWords = widget.searchKeyWords;
    _search(pageNumber);
    super.initState();
  }

  void _search(int pageNumber) {
    var data = {"k": searchKeyWords};
    ApiService().search(pageNumber, data, (ArticleBean articleBean) {
      setState(() {
        if (articleBean.data != null) {
          isLoadAll = articleBean.data.over;
          _articleList.addAll(articleBean.data.articles);
        }
        if (isLoadAll) {
          _articleList.add(null);
        }
        _updateLocalSearch();
      });
    });
  }

  void _updateLocalSearch() {
    List<String> localSearchList =
        SpUtil().getStringList(Constant.localSearchHistoryKey);
    if (localSearchList == null) {
      localSearchList = List();
    }
    if (!localSearchList.contains(searchKeyWords)) {
      localSearchList.add(searchKeyWords);
    }
    SpUtil().putStringList(Constant.localSearchHistoryKey, localSearchList);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
