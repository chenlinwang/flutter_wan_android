import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/home/home.dart';
import 'package:flutter_app/model/article_bean.dart';
import 'package:flutter_app/res/strings.dart';
import 'package:flutter_app/webview/WebViewPage.dart';
import 'package:flutter_app/widget/my_circular_progress_indicator.dart';

class WeChatListPage extends StatefulWidget {
  var _id;

  WeChatListPage(int id) {
    this._id = id;
  }

  @override
  _WeChatListPageState createState() => _WeChatListPageState(_id);
}

class _WeChatListPageState extends State<WeChatListPage> {
  int _id;

  _WeChatListPageState(this._id);

  List<Article> _articleList = List();
  bool _isLoadAll;
  int _pageNumber = 0;

  void getWeChatArticleList(int pageNumber) {
    ApiService().getWeChatArticleData(_id, pageNumber,
        (ArticleBean articleBean) {
      setState(() {
        if (articleBean.data != null) {
          _isLoadAll = articleBean.data.over;
          _articleList.addAll(articleBean.data.articles);
        }
        if (_isLoadAll) {
          _articleList.add(null);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView.builder(
            itemBuilder: (context, index) {
              if (index == _articleList.length - 1) {
                if (!_isLoadAll) {
                  getWeChatArticleList(_pageNumber);
                  return MyCircularProgressIndicator();
                } else {
                  return Container(
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.center,
                    child: Text(Strings.IS_LOAD_ALL_ARTICLE_CN),
                  );
                }
              } else {
                return GestureDetector(
                  child: ArticleListItemWidget(_articleList[index]),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                WebViewPage(_articleList[index].link)));
                  },
                );
              }
            },
            itemCount: _articleList.length),
      ),
    );
  }

  @override
  void initState() {
    getWeChatArticleList(_pageNumber);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
