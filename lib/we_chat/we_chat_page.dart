import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/model/article_category_bean.dart';
import 'package:flutter_app/we_chat/we_chat_list_page.dart';
import 'package:flutter_app/widget/my_circular_progress_indicator.dart';

class WeChatPage extends StatefulWidget {
  @override
  WeChatPageState createState() => new WeChatPageState();
}

class WeChatPageState extends State<WeChatPage> {
  var _tabList = List<Tab>();
  var _tabBarView = List<Widget>();

  void _getWeChatData() async {
    ApiService()
        .getWeChatArticleCategory((ArticleCategoryBean articleCategoryBean) {
      setState(() {
        _tabList.clear();
        _tabBarView.clear();
        for (var bean in articleCategoryBean.category) {
          _tabList.add(Tab(text: bean.name));
          _tabBarView.add(WeChatListPage(bean.id));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_tabList.length == 0) {
      return MyCircularProgressIndicator();
    } else {
      return MaterialApp(
        home: DefaultTabController(
          length: _tabList.length,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(50.0),
              child: AppBar(
                bottom: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.white.withOpacity(0.5),
                  indicatorColor: Colors.white,
                  tabs: _tabList,
                ),
              ),
            ),
            body: TabBarView(
              children: _tabBarView,
            ),
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    _getWeChatData();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
