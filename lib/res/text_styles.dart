import 'package:flutter/cupertino.dart';

import 'colours.dart';

class TextStyles {
  static TextStyle size18AndBoldText = TextStyle(
      fontSize: 18, fontWeight: FontWeight.bold, color: Colours.app_theme);
  static TextStyle size16AndBoldText = TextStyle(
      fontSize: 16, fontWeight: FontWeight.bold, color: Colours.gray_66);
}
