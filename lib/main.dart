import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/project/project_article_category.dart';
import 'package:flutter_app/res/colours.dart';
import 'package:flutter_app/res/strings.dart';
import 'package:flutter_app/tools/CustomScrollViewTestRoute.dart';
import 'package:flutter_app/tools/DialogRoute.dart';
import 'package:flutter_app/tools/GestureDetectorTestRoute.dart';
import 'package:flutter_app/tools/InheritedWidgetTestRoute.dart';
import 'package:flutter_app/tools/ScrollControllerTestRoute.dart';
import 'package:flutter_app/tools/_Drag.dart';
import 'package:flutter_app/tools/my_tools.dart';
import 'package:flutter_app/utils/sp_util.dart';
import 'package:flutter_app/we_chat/we_chat_page.dart';

import 'home/home.dart';
import 'knowledge_system/knowledge_system_page.dart';
import 'login/login_page.dart';
import 'me/me_page.dart';
import 'mine/mine.dart';

Future<void> main() async {
  //屏幕适配
  WidgetsFlutterBinding.ensureInitialized();
  await SpUtil().init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

/// https://blog.csdn.net/u011578734/article/details/108774862
// class _MyHomePageState extends State<MyHomePage> {
//   int bottomSelect = 0;
//   List<String> tabNames = ["主页", "图片", "我的"];
//   List<Color> colorList = [Colors.cyan, Colors.deepPurpleAccent];
//   var currentTab = "主页";
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       //显示在界面顶部的一个 AppBar。
//       appBar: AppBar(
//         backgroundColor: Colors.green,
//         //设置标题栏的背景颜色
//         title: Container(
//           decoration: BoxDecoration(
//               color: Colors.amberAccent,
//               border: Border.all(color: Colors.blue, width: 2),
//               gradient: LinearGradient(
//                   colors: colorList,
//                   begin: Alignment.topLeft,
//                   end: Alignment.topRight)),
//           child: Column(
//             children: <Widget>[
//               Builder(
//                   builder: (BuildContext context) => InkWell(
//                       child: Title(
//                         child: new Text(
//                           '这是一个标题',
//                           style: new TextStyle(
//                             fontSize: 20.0,
//                             color: Colors.white,
//                           ),
//                         ),
//                         color: Colors.white,
//                       ),
//                       onTap: () {
//                         Scaffold.of(context).showSnackBar(
//                             new SnackBar(content: new Text('点击')));
//                       })),
//             ],
//           ),
//         ),
//         centerTitle: true,
//         //设置标题居中
//         elevation: 10.0,
//         //设置标题栏下面阴影的高度
//         // leading: new Builder(builder: (BuildContext context) {
//         //   return new GestureDetector(
//         //       //设置事件
//         //       child: new Icon(
//         //         //设置左边的图标
//         //         Icons.account_circle, //设置图标内容
//         //         color: Colors.white, //设置图标的颜色
//         //       ),
//         //       onTap: () {
//         //
//         //       },
//         //       onLongPress: () {
//         //         Scaffold.of(context)
//         //             .showSnackBar(new SnackBar(content: new Text('长按')));
//         //       },
//         //       onDoubleTap: () {
//         //         Scaffold.of(context)
//         //             .showSnackBar(new SnackBar(content: new Text('双击')));
//         //       },
//         //       onSecondaryTapUp: (TapUpDetails details) {
//         //         Scaffold.of(context)
//         //             .showSnackBar(new SnackBar(content: new Text(details.globalPosition.toString())));
//         //       });
//         // }),
//         brightness: Brightness.light,
//         //设置状态栏的字体颜色(白色/黑色)
//         primary: true,
//         //是否设置内容避开状态栏
//         actions: <Widget>[
//           new Padding(
//             child: new Icon(Icons.add, color: Colors.white),
//             padding: EdgeInsets.all(10.0),
//           ),
//         ],
//       ),
//       body: Column(
//         // crossAxisAlignment: CrossAxisAlignment.start,
//         //MainAxisAlignment 主轴方向的对齐方式
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: [
//            Container(
//               // color: Colors.amberAccent,
//               width: 200,
//               height: 200,
//              decoration: BoxDecoration(
//                color: Colors.white,
//                border:Border.all(
//                  color: Colors.grey,
//                  width: 2.0
//                ),
//                borderRadius: BorderRadius.all(Radius.circular(10)),
//                image: DecorationImage(image: AssetImage('images/ic_like.png'))
//              ),
//             ),
//           Container(
//               color: Colors.purple,
//               padding: EdgeInsets.all(10.0),
//               child: Row(
//                 children: [
//                   Expanded(
//                     child: Container(
//                       color: Colors.blue,
//                       width: 100,
//                       height: 100,
//                     ),
//                   ),
//                   Container(
//                     color: Colors.deepPurpleAccent,
//                     width: 100,
//                     height: 100,
//                   ),
//                 ],
//               ),
//             ),
//            Container(
//                 color: Colors.red,
//                 width: 100,
//                 height: 100,
//               ),
//         ],
//       ),
//       //底部固定的widget
//       persistentFooterButtons: <Widget>[
//         new Icon(Icons.add_shopping_cart),
//       ],
//       //侧边栏
//       drawer: new Drawer(
//           child: ListView.builder(
//               itemCount: listCount,
//               itemBuilder: (context, index) {
//                 return new Card(
//                     child: new ListTile(
//                   leading: new Icon(Icons.phone),
//                   title: new Text("我是抽屉菜单 $index"),
//                   subtitle: new Text("010-12345678"),
//                   trailing: new Icon(Icons.arrow_forward_ios),
//                   contentPadding: EdgeInsets.symmetric(horizontal: 20.0),
//                 ));
//               })),
//       //底部导航栏
//       bottomNavigationBar: BottomNavigationBar(
//         currentIndex: bottomSelect, //默认选中的位置
//         fixedColor: Colors.green, //选中的颜色
//         items: <BottomNavigationBarItem>[
//           new BottomNavigationBarItem(
//             icon: new Icon(
//               Icons.home,
//             ),
//             activeIcon: new Icon(
//               //选中显示的icon
//               Icons.home,
//             ),
//             title: new Text(
//               tabNames[0],
//             ),
//           ),
//           new BottomNavigationBarItem(
//             icon: new Icon(
//               Icons.picture_in_picture,
//             ),
//             title: new Text(
//               tabNames[1],
//             ),
//           ),
//           new BottomNavigationBarItem(
//             icon: new Icon(
//               Icons.account_box,
//             ),
//             title: new Text(
//               tabNames[2],
//             ),
//           ),
//         ],
//         onTap: (int index) {
//           setState(() {
//             bottomSelect = index;
//             currentTab = tabNames[index];
//             print(index.toString());
//           });
//         },
//       ),
//     );
//   }
//
//   int listCount = 10; //当前列表显示的子item数量
// }

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  DateTime _lastPressedAt; //上次点击时间
  final List<Widget> _children = [
    Home(),
    ProjectPage(),
    WeChatPage(),
    KnowLedgeSystemPage(),
    Mine(),
    // MePageState(),//基础widget
    // MyTools(),//ListView
    // CustomScrollViewTestRoute(), //滚动组件
    // ScrollControllerTestRoute(),//滚动监听及控制
    // InheritedWidgetTestRoute(),//数据共享（InheritedWidget）
    // DialogRoute(),//对话框
    // GestureDetectorTestRoute(),//手势识别
    // GestureDetectorDrag(), //手势滑动
    // LoginPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.home),
              title: Text(Strings.HOME_CN)),
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.present_to_all),
              title: Text(Strings.PROJECT_CN)),
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.chat),
              title: Text(Strings.WE_CHAT_CN)),
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.system_update),
              title: Text(Strings.KNOWLEDGE_SYSTEM_CN)),
          BottomNavigationBarItem(
              // ignore: deprecated_member_use
              icon: Icon(Icons.cached),
              title: Text(Strings.ME_CN)),
        ],
        selectedItemColor: Colours.app_theme.withOpacity(0.5),
        unselectedItemColor: Colors.white,
        selectedLabelStyle: TextStyle(color: Colours.app_theme),
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.grey[400],
      ),
      body: _children[_currentIndex],
      // bottomSheet: WillPopScope(
      //     onWillPop: () async {
      //       if (_lastPressedAt == null ||
      //           DateTime.now().difference(_lastPressedAt) >
      //               Duration(seconds: 1)) {
      //         //两次点击间隔超过1秒则重新计时
      //         _lastPressedAt = DateTime.now();
      //         return false;
      //       }
      //       return true;
      //     },
      //     child: Container(
      //       alignment: Alignment.center,
      //       child: Text("1秒内连续按两次返回键退出"),
      //     )
      // )
    );
  }
}
