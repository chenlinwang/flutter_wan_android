import 'package:flutter/material.dart';

class GestureDetectorDrag extends StatefulWidget {
  @override
  _GestureDetectorDragState createState() => new _GestureDetectorDragState();
}

class _GestureDetectorDragState extends State<GestureDetectorDrag> {
  double _top = 0.0; //距顶部的偏移
  double _left = 0.0; //距左边的偏移
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
            top: _top,
            left: _left,
            child: GestureDetector(
              child: CircleAvatar(child: Text("A")),
              onPanDown: (DragDownDetails e){
                //打印手指按下的位置（相对于屏幕）
                print("用户手指按下：${e.globalPosition}");
              },
              //手指滑动时会触发次回调
              onPanUpdate: (DragUpdateDetails e){
                //手指滑动时，更新偏移，重新构建
                setState(() {
                  _left+=e.delta.dx;
                  _top+=e.delta.dy;
                });
              },
              onPanEnd: (DragEndDetails e){
                //打印滑动结束时在x、y轴上的速度
                print(e.velocity);
              },
            )),
      ],
    );
  }
}
