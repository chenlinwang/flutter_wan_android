import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/mine/integral_rank_page.dart';
import 'package:flutter_app/model/coin_bean.dart';
import 'package:flutter_app/res/strings.dart';
import 'package:flutter_app/utils/constant.dart';
import 'package:flutter_app/utils/sp_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class PersonalInfoPage extends StatefulWidget {
  @override
  PersonalInfoPageState createState() => new PersonalInfoPageState();
}

class PersonalInfoPageState extends State<PersonalInfoPage> {
  File _image;
  Coin _coin;

  @override
  void initState() {
    String avatarPath = SpUtil().getString(Constant.avatarPathTag);
    if (avatarPath != null) {
      _image = File(avatarPath);
    }
    super.initState();
  }

  void _getPersonInfo() {
    ApiService().getPersonalCoinData((CoinBean coinBean) {
      setState(() {
        _coin = coinBean.coin;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('个人信息'),
      ),
      body: Column(
        children: [
          InkWell(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                      child: Text(
                    "头像",
                    style: TextStyle(fontSize: 18, color: Colors.grey),
                  )),
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    width: 40,
                    height: 40,
                    child: _image == null
                        ? CircleAvatar(
                            backgroundImage: AssetImage("images/landscape.jpg"),
                          )
                        : Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: FileImage(_image))),
                          ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
            onTap: () {
              _showModifyAvatarDialog(context);
            },
          ),
          _divideLine(),
          InkWell(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                children: [
                  Expanded(
                      child: Text(
                    "用户名",
                    style: TextStyle(fontSize: 18, color: Colors.grey),
                  )),
                  Text(
                    SpUtil().getString(Constant.usernameTag),
                    style: TextStyle(fontSize: 18, color: Colors.black),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                  )
                ],
              ),
            ),
          ),
          _itemInfo("总积分", "56", false),
          _itemInfo("当前排名", "10086", false),
          _itemInfo("当前等级", "1", false),
          _itemInfo("积分列表", "", true),
          _itemInfo("积分排行榜", "", true),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _itemInfo(String name, String value, bool isVisible) {
    return InkWell(
      child: Column(children: [
        _divideLine(),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            children: [
              Expanded(
                  child: Text(
                name,
                style: TextStyle(fontSize: 18, color: Colors.grey),
              )),
              Text(
                value,
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
              Visibility(
                visible: isVisible,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ]),
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => IntegralRankPage()));
      },
    );
  }

  Future<void> _showModifyAvatarDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(Strings.PLS_SELECT_PICTURE_CN),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text("相册"),
                    onTap: () {
                      _openGallery();
                      Navigator.of(context).pop();
                    },
                  ),
                  Padding(padding: EdgeInsets.all(8.0)),
                  GestureDetector(
                    child: Text("拍照"),
                    onTap: () {
                      _openCamera();
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  void _openGallery() async {
    final pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    SpUtil().putString(Constant.avatarPathTag, pickedFile.path);
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  void _openCamera() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    SpUtil().putString(Constant.avatarPathTag, pickedFile.path);
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  Widget _divideLine() {
    return Container(
      height: 1,
      margin: EdgeInsets.symmetric(vertical: 10),
      color: Colors.grey,
    );
  }
}
