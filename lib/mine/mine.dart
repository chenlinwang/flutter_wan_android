import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/login/login_page.dart';
import 'package:flutter_app/mine/collection_page.dart';
import 'package:flutter_app/mine/personnal_info_page.dart';
import 'package:flutter_app/model/user_bean.dart';
import 'package:flutter_app/res/strings.dart';
import 'package:flutter_app/res/text_styles.dart';
import 'package:flutter_app/utils/constant.dart';
import 'package:flutter_app/utils/sp_util.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Mine extends StatefulWidget {
  @override
  _MineState createState() => new _MineState();
}

class _MineState extends State<Mine> {
  String _isLogin;

  void _refreshLoginStatus() {
    setState(() {
      if (SpUtil().getBool(Constant.isLoginKey) == null ||
          !SpUtil().getBool(Constant.isLoginKey)) {
        _isLogin = Strings.LOGIN_CN;
      } else {
        _isLogin = Strings.LOGOUT_CN;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        children: [
          Stack(
            alignment: AlignmentDirectional.topCenter,
            overflow: Overflow.visible,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 3,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    image: DecorationImage(
                        image: AssetImage("images/landscape.jpg"),
                        colorFilter: ColorFilter.mode(
                            Colors.black.withOpacity(0.7), BlendMode.dstATop),
                        fit: BoxFit.fill)),
              ),
              Positioned(
                child: GestureDetector(
                  child: Column(
                    children: [
                      Container(
                          width: 100,
                          height: 100,
                          child:
                              SpUtil().getString(Constant.avatarPathTag) == null
                                  ? CircleAvatar(
                                      backgroundImage:
                                          AssetImage("images/landscape.jpg"),
                                    )
                                  : Container(
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: FileImage(File(SpUtil()
                                                .getString(
                                                    Constant.avatarPathTag))),
                                          )),
                                    )),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Text(
                          SpUtil().getString(Constant.usernameTag) == null
                              ? Strings.USER_NAME_CN
                              : SpUtil().getString(Constant.usernameTag),
                          style: TextStyle(fontSize: 25, color: Colors.white),
                        ),
                      ),
                      Container(width: 250, height: 0.5, color: Colors.white)
                    ],
                  ),
                  onTap: () {
                    print(SpUtil().getBool(Constant.isLoginKey));
                    if (SpUtil().getBool(Constant.isLoginKey) == null ||
                        !SpUtil().getBool(Constant.isLoginKey)) {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()))
                          .then((value) => {
                                if (value == "loginSuccessful")
                                  _refreshLoginStatus(),
                              });
                    } else {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PersonalInfoPage()));
                    }
                  },
                ),
                bottom: 50,
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Visibility(
            visible: SpUtil().getBool(Constant.isLoginKey) == null
                ? false
                : SpUtil().getBool(Constant.isLoginKey),
            child: GestureDetector(
              child: _infoItem(Icons.favorite, "我的收藏"),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MyCollectionPage()));
              },
            ),
          ),
          GestureDetector(
            child: _infoItem(Icons.info, "登出"),
            onTap: () {
              if (SpUtil().getBool(Constant.isLoginKey) == null ||
                  !SpUtil().getBool(Constant.isLoginKey)) {
                Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()))
                    .then((value) => {
                          if (value == "loginSuccessful") _refreshLoginStatus()
                        });
              } else {
                SpUtil().putBool(Constant.isLoginKey, false);
                SpUtil().remove(Constant.usernameTag);
                SpUtil().remove(Constant.avatarPathTag);
                SpUtil().remove(Constant.cookieListKey);
                ApiService().logout((UserBean userBean) {
                  if (userBean.errorCode == 0) {
                    Fluttertoast.showToast(
                        msg: Strings.QUIT_LOGIN_CN, textColor: Colors.grey);
                    _refreshLoginStatus();
                  }
                });
              }
            },
          ),
        ],
      ),
    );
  }
}

Widget _infoItem(IconData icon, String text) {
  return Padding(
    padding: EdgeInsets.all(15),
    child: Row(
      children: [
        Icon(
          icon,
          size: 30,
        ),
        Expanded(
            child: Padding(
                padding: EdgeInsets.only(top: 5, bottom: 5, left: 20),
                child: Text(text,
                    style: TextStyle(fontSize: 20, color: Colors.grey)))),
        Icon(Icons.arrow_forward)
      ],
    ),
  );
}
