import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/model/article_category_bean.dart';
import 'package:flutter_app/widget/my_circular_progress_indicator.dart';

import 'knowledge_list_page.dart';

class KnowLedgeSystemPage extends StatefulWidget {
  @override
  _KnowLedgeSystemPageState createState() => new _KnowLedgeSystemPageState();
}

class _KnowLedgeSystemPageState extends State<KnowLedgeSystemPage> {
  ArticleCategoryBean _articleCategoryBean;

  void getKnowledgeList() async {
    ApiService().getKnowledgeSystemArticleCategory(
        (ArticleCategoryBean articleCategoryBean) {
      setState(() {
        _articleCategoryBean = articleCategoryBean;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_articleCategoryBean == null ||
        _articleCategoryBean.category.length == 0) {
      return Center(
        child: MyCircularProgressIndicator(),
      );
    } else {
      return Scaffold(
        body: SafeArea(
          child: ListView.builder(
            itemBuilder: (context, index) =>
                KnowLedgeSystemItem(_articleCategoryBean.category[index]),
            itemCount: _articleCategoryBean.category.length,
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    getKnowledgeList();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}

// ignore: must_be_immutable
class KnowLedgeSystemItem extends StatelessWidget {
  Category _category;

  KnowLedgeSystemItem(this._category);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(_category.name),
      children: [
        ListView.builder(
            itemCount: _category.children.length,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            controller: ScrollController(),
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.all(14),
                child: Card(
                  clipBehavior: Clip.antiAlias,
                  color: Theme.of(context).cardTheme.color,
                  elevation: 30,
                  shadowColor: Colors.black.withOpacity(0.4),
                  margin: EdgeInsets.only(right: 25),
                  child: InkWell(
                    splashColor: Theme.of(context).primaryColor,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => KnowledgeSystemArticleListPage(
                                  _category.children[index].name,
                                  _category.children[index].id)));
                    },
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        _category.children[index].name,
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                ),
              );
            })
      ],
    );
  }
}
