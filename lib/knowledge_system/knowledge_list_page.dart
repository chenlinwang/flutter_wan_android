import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/api/api_service.dart';
import 'package:flutter_app/home/home.dart';
import 'package:flutter_app/model/article_bean.dart';
import 'package:flutter_app/res/strings.dart';
import 'package:flutter_app/webview/WebViewPage.dart';
import 'package:flutter_app/widget/my_circular_progress_indicator.dart';

class KnowledgeSystemArticleListPage extends StatefulWidget {
  var _titleName;
  var _id;

  KnowledgeSystemArticleListPage(this._titleName, this._id);

  @override
  KnowledgeSystemArticleListPageState createState() =>
      new KnowledgeSystemArticleListPageState(_titleName, _id);
}

class KnowledgeSystemArticleListPageState
    extends State<KnowledgeSystemArticleListPage> {
  var _titleName;
  var _id;
  var pageNumber = 0;
  bool isLoadAll = false;
  List<Article> _list = List<Article>();

  KnowledgeSystemArticleListPageState(this._titleName, this._id);

  void getArticleList(var pageNumber) {
    ApiService().getKnowledgeSystemArticleData(pageNumber, _id,
        (ArticleBean articleBean) {
      setState(() {
        if (articleBean.data != null) {
          isLoadAll = articleBean.data.over;
          _list.addAll(articleBean.data.articles);
        }
        if (isLoadAll) {
          _list.add(null);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_titleName),
        centerTitle: true,
      ),
      body: (_list.length == 0)
          ? Center(child: MyCircularProgressIndicator())
          : SafeArea(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  if (index == _list.length - 1) {
                    if (!isLoadAll) {
                      getArticleList(++pageNumber);
                      return MyCircularProgressIndicator();
                    } else {
                      return Container(
                        padding: EdgeInsets.all(5),
                        alignment: Alignment.center,
                        child: Text(Strings.IS_LOAD_ALL_ARTICLE_CN),
                      );
                    }
                  } else {
                    return GestureDetector(
                      child: ArticleListItemWidget(_list[index]),
                      onTap: () {
                        print(_list[index].link.trim().toString());
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    WebViewPage(_list[index].link)));
                      },
                    );
                  }
                },
                itemCount: _list.length,
              ),
            ),
    );
  }

  @override
  void initState() {
    getArticleList(pageNumber);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
